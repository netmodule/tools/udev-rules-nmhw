#!/bin/sh

wait_on_free_eth0 () {
	while ifconfig eth0 > /dev/null 2>/dev/null; do
		sleep 0.1
	done
}

rename_lan0_to_eth0() {
	ip link set lan0 down
	ip link set lan0 name eth0
	ip link set eth0 up
}

wait_on_free_eth0
rename_lan0_to_eth0
exit 0
